#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <stdlib.h> 
#include <MemoryFree.h>

#define DEBUG                       true            // tryb testowy
#define RESPONSE_TIMEOUT            2000            // czas oczekiwania na odpowiedz

class Device {
public:
    //    String labels[3] = {"id", "status", "value"};
    //    String type;
    int id;
    int pinOut;
    String description;
    String value;
    String lastModification;

    construct(String description, int pinOut) {
        description = description;
        pinOut = pinOut;
    }
    virtual int printArea() = 0;
};

class Rectangle : public Device {
public:
    String type;
    int length;
    int breadth;

    Rectangle(int l, int b) {
        length = l;
        breadth = b;
    }

    int printArea() {
        return length * breadth;
    }
};

int devicesAmount = 0;
Device * devicesArray [20];
const byte numChars = 400;
char globalJsonResponse[numChars];
boolean newData = false;

SoftwareSerial ESP8266(2, 3); // RX=2, TX=3 dla ESP8266

/**
 * Ustawienia WiFI oraz REST-API
 */
int LIGHT_KITCHEN = 4; // swiatlo w kuchni
int LIGHT_DINING_ROOM = 5; // swiatlo w jadalni
int LIGHT_LIVING_ROOM = 6; // swiatlo w salonie
int LIGHT_BEDROOM = 7; // swiatlo w sypialni
int LIGHT_ENGINE_WORKING_STATUS = 8; // dioda informująca o pracy silnika
char * MY_SSID = "";
char * MY_PASSWORD = "";
char * HOST = "myhomewitharduino.vipserv.org";
char * REST_API = "/app/rest-api?";

void setup() {

    if (DEBUG) {
        HOST = "192.168.1.110";
        REST_API = "/app/rest-api";
    }

    Serial.begin(9600);
    dump("Serial Arduino otwarty!");
    ESP8266.begin(9600);
    dump("Software Serial otwarty!");

    pinMode(LIGHT_KITCHEN, OUTPUT);
    digitalWrite(LIGHT_KITCHEN, LOW);
    pinMode(LIGHT_DINING_ROOM, OUTPUT);
    digitalWrite(LIGHT_DINING_ROOM, LOW);
    pinMode(LIGHT_LIVING_ROOM, OUTPUT);
    digitalWrite(LIGHT_LIVING_ROOM, LOW);
    pinMode(LIGHT_BEDROOM, OUTPUT);
    digitalWrite(LIGHT_BEDROOM, LOW);
    pinMode(LIGHT_ENGINE_WORKING_STATUS, OUTPUT);
    digitalWrite(LIGHT_ENGINE_WORKING_STATUS, LOW);

    /* Wstępna konfiguracja ESP-01*/
    sendToWifi("AT+RST"); // reset modulu
    sendToWifi("AT+CWMODE=1"); // configure as access point
    sendToWifi("AT+CIFSR"); // get ip address
    connectEspToWiFi();
    getDevicesAmount();
    Serial.println("devicesAmount: ");
    Serial.println(devicesAmount);
    prepareAllDevices();
}

void loop() {
    if (Serial.available()) {
        unoCommandController(Serial.readString());
    }
    if (ESP8266.available()) {
        ESP8266CommandController(readWifiSerialMessage());
    }
    //    testConection();
    //    delay(1000);
    //    testConection2();
    //    delay(1000);
}

/**
 * Kontroller instrukcji Arduino Uno
 * @var String message
 * @return void
 */
void unoCommandController(String message) {
    dump("ArduinoUno CommandController message: ");
    dump(message);

    if (find(message, "AT")) {
        // Testowanie komend AT wpisanych z monitora portu szeregowego Arduino UNO.
        //        ESP8266.println(message);
        String espCommand = message;
        sendToWifi(espCommand);
    } else if (find(message, "ESP+")) {
        // Testowanie komend dla ESP wpisanych z monitora portu szeregowego Arduino Uno
        String espCommand = message.substring(4, message.length());
        ESP8266.println(espCommand);
    } else if (find(message, "UNO+")) {
        // Testowanie komend dla Arduino Uno wpisanych z monitora portu szeregowego Arduino Uno
        String unoCommand = message.substring(4, message.length());
        if (find(unoCommand, "LIGHT+")) { // ŚWIATŁA
            String lightId = unoCommand.substring(6, unoCommand.length());
            UnoSwitchLight(lightId.toInt());
        } else if (find(unoCommand, "SHUTTER+")) { // ROLETY
            String shutterId = unoCommand.substring(8, unoCommand.length());
            UnoSwitchShutter(shutterId.toInt());
        } else {
            //            Serial.println("UNO+ undefined sub command: " + message);
        }
    } else {
        // Nierozpoznano polecenia
        //        Serial.println("UNO undefined command: " + message);
    }
}

/**
 * Kontroller instrukcji ESP-01
 * @var String message
 * @return void
 */
void ESP8266CommandController(String message) {
    if (DEBUG) {
        dump("ESP8266 CommandController message: ");
        dump(message);
    }

    if (find(message, "AT")) {
        String espResult = sendToWifi(message);
        Serial.println(espResult);
    } else if (find(message, "HELLO")) { //receives HELLO from wifi
        Serial.println("HI!"); //arduino says HI
    } else {
        // Nierozpoznano polecenia
        //        dump("ESP undefined command: " + message);
    }
}

/**
 * Przygotowanie i sprawdzenie połączenia z siecią WiFi
 */
void connectEspToWiFi() {
    delay(3000);
    ESP8266.println("AT+CWJAP?");
    if (ESP8266.find("OK")) {
        dump("WiFi already connected!");
    } else {
        dump("************************Connection***************************");
        String espResponse = sendToWifi("AT+CWJAP=\"" + String(MY_SSID) + "\",\"" + String(MY_PASSWORD) + "\"");
        connectEspToWiFi();
    }
}

void getDevicesAmount() {
    String json = getFromRestApi("getDevicesAmount");
    String str = jsonToString(json);
    devicesAmount = jsonToString(json).toInt();
};

void prepareAllDevices() {
    if (DEBUG) {
        Serial.print(" BEFORE freeMemory()=");
        Serial.println(freeMemory());
    }
    for (int i = 0; i < devicesAmount; i++) {
        dump("SET Device " + String(i) + ": ");
        devicesArray[i] = new Rectangle(i + 1, 2);
        if (DEBUG) {
            Serial.print(" AFTER freeMemory()=");
            Serial.println(freeMemory());
            Serial.println(sizeof (globalJsonResponse));
        }
    }

    for (int j = 0; j < devicesAmount; j++) {
        dump("Print Device " + String(j) + ": ");
        Serial.println(devicesArray[j]->printArea());
    }
}

/**
 * Aktualizacja urządzenia w RES-API
 */
void setDeviceInRestApi(String message) {
    prepareConnectionTCP();
}

void testConection() {
    if (prepareConnectionTCP()) {
        //        String request = "GET " + prepareRestApiRequest("getBlind", LIGHT_KITCHEN);
        String request = "GET " + prepareRestApiRequest("getAllDevices");
        dump(request);
        String len = String(request.length() + 2);
        ESP8266.println("AT+CIPSEND=" + len);
        if (ESP8266.find(">")) {
            dump("Wysyłanie...");
            ESP8266.println(request);
            if (ESP8266.find("SEND OK")) {
                dump("Pakiet 1 wysłany");
                String response = readWifiSerialMessage();
                //                convertJsonResponse(response, "1");
            }
        }
    } else {
        dump("Test 1 FAIL");
    }
}

void testConection2() {
    if (prepareConnectionTCP()) {
        //        String request = "GET " + prepareRestApiRequest("getBlind", 3);
        String request = "GET " + prepareRestApiRequest("getAllDevices");
        dump(request);
        String len = String(request.length() + 2);
        ESP8266.println("AT+CIPSEND=" + len);
        if (ESP8266.find(">")) {
            dump("Wysyłanie...");
            ESP8266.println(request);
            if (ESP8266.find("SEND OK")) {
                dump("Pakiet 2 wysłany");
                while (!ESP8266.find("+")) {
                }
                readJsonMessage();
                //                convertJsonResponse(globalJsonResponse, "2");
                //                showNewData();
                printJsonMessage();
            }
        }
    } else {
        dump("Test 2 FAIL");
    }
}

/**
 * Odczyt z REST-API
 */
String getFromRestApi(String methot) {
    return getFromRestApi(methot, "", "");
}

/**
 * Odczyt z REST-API
 */
String getFromRestApi(String methot, String param1, String param2) {
    if (prepareConnectionTCP()) {
        String request = "GET " + prepareRestApiRequest(methot, param1, param2);
        dump("Request: " + request);
        String len = String(request.length() + 2);
        ESP8266.println("AT+CIPSEND=" + len);
        if (ESP8266.find(">")) {
            dump("Wysyłanie...");
            ESP8266.println(request);
            if (ESP8266.find("SEND OK")) {
                dump("Pakiet wysłany");
                //                String json = readWifiSerialMessage();
                //                dump(json.length());
                readJsonMessage();
                return globalJsonResponse;
            }
        }
    } else {
        dump("COś nie pykło");
        return "FAIL";
    }
}

/**
 * Nawiązanie połączenia TCP z REST-API
 */
boolean prepareConnectionTCP() {
    ESP8266.println("AT+CIPSTART=\"TCP\",\"" + String(HOST) + "\",80");
    String espResponse = readWifiSerialMessage();
    if (find(espResponse, "OK")) {
        dump("TCP connection ready 1");
        return true;
    } else if (find(espResponse, "ALREADY CONNECTED")) {
        dump("TCP connection ready 2");
        return true;
    } else if (find(espResponse, "CONNECT")) {
        dump("TCP connection ready 3");
        return true;
    } else {
        dump("Nie wiem co się stało - wyświetl to na konsoli.");
        dump(espResponse);
        return false;
    }
}

/**
 * Zamknięcie połączenia TCP
 */
void closeConnectionToRestApi() {
    ESP8266.println("AT+CIPCLOSE");
}

/**
 * Wysyłanie ciągu znaków do połaczenia TCP z użyciem AT+CIPSEND
 * @var String str
 * @return void
 */
void sendData(String str) {
    String len = "";
    len += str.length();
    sendToWifi("AT+CIPSEND=0," + len);
    delay(100);
    sendToWifi(str);
    delay(100);
    sendToWifi("AT+CIPCLOSE=5");
}

/*
  Name: find
  Description: Porównaj ciagi znakow
  Params:
  Returns: true if match else false
 */
boolean find(String inString, String findValue) {
    return inString.indexOf(findValue) >= 0;
}


/*
  Name: readSerialMessage
  Description: Function used to read data from Arduino Serial.
  Params:
  Returns: The response from the Arduino (if there is a reponse)
  String  readSerialMessage() {
  //  Serial.println("readSerialMessage");
  char value[100];
  int index_count = 0;
  while (Serial.available() > 0) {
    value[index_count] = Serial.read();
    index_count++;
    value[index_count] = '\0'; // Null terminate the string
  }
  String str(value);
  str.trim();
  return str;
  }
 */

/**
 * Odczytuje z ESP znak po znaku i zwraca połączoną wiadomość
 * @return String
 */
String readWifiSerialMessage() {
    if (DEBUG) {
        dump(" BEFORE freeMemory()=");
        dump(freeMemory());
    }
    String response = "";
    long int time = millis();
    while ((time + RESPONSE_TIMEOUT) > millis()) {
        while (ESP8266.available()) {
            char c = ESP8266.read();
            Serial.print(c);
            response += c;
        }
    }
    response.trim();
    if (DEBUG) {
        dump(" AFTER freeMemory()=");
        dump(freeMemory());
        dump(response);
    }
    return response;
}

void * readJsonMessage() {
    if (DEBUG) {
        Serial.print(" BEFORE freeMemory()=");
        Serial.println(freeMemory());
    }
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;
    long int time = millis();
    while ((time + RESPONSE_TIMEOUT) > millis()) {
        while (ESP8266.available() > 0) {
            rc = ESP8266.read();
            if (recvInProgress == true) {
                if (rc != endMarker) {
                    globalJsonResponse[ndx] = rc;
                    ndx++;
                    if (ndx >= numChars) {
                        ndx = numChars - 1;
                    }
                } else {
                    globalJsonResponse[ndx] = '\0'; // terminate the string
                    recvInProgress = false;
                    ndx = 0;
                }
            } else if (rc == startMarker) {
                recvInProgress = true;
            }
        }
    }
    if (DEBUG) {
        Serial.print(" AFTER freeMemory()=");
        Serial.println(freeMemory());
        Serial.println(sizeof (globalJsonResponse));
    }
}

String jsonToString(String json) {
    int start = json.indexOf("{");
    int end = json.indexOf("}") + 1;
    return json.substring(start, end);
}

/*
  Name: sendToWifi
  Description: Funkcja wysyls informacje do ESP8266 i odczytuje odpowiedz z serialportu ESP8266
  Params: command - the data/command to send; timeout - the time to wait for a response; debug - print to Serial window?(true = yes, false = no)
  Returns: The response from the esp8266 (if there is a reponse)
 */
String sendToWifi(String espCommand) {
    String espResponse = "";
    ESP8266.println(espCommand);
    long int time = millis();
    while ((time + RESPONSE_TIMEOUT) > millis()) {
        while (ESP8266.available()) {
            char c = ESP8266.read();
            espResponse += c;
        }
    }
    dump("sendToWifi espResponse: " + espResponse);
    return espResponse;
}

/**
 * sendToUno
 * Function used to send data to ESP8266.
 * @var String command
 * @return: The response from the esp8266 (if there is a reponse)
 */
String sendToUno(String command) {
    String response = "";
    Serial.println(command); // send the read character to the esp8266
    unsigned long int time = millis();
    while ((time + RESPONSE_TIMEOUT) > millis()) {
        while (Serial.available()) {
            // The esp has data so display its output to the serial window
            char c = Serial.read(); // read the next character.
            response += c;
        }
    }
    dump("sendToUno: " + response);
    return response;
}

/**
 * prepareRestApiRequest
 * Funkcja zwraca gotowy adres zapytania do REST-API
 * @var String action
 */
String prepareRestApiRequest(String actionID) {
    return prepareRestApiRequest(actionID, "", "");
}

/**
 * prepareRestApiRequest
 * Funkcja zwraca gotowy adres zapytania do REST-API
 * @var String action
 * @var int deviceID
 */
String prepareRestApiRequest(String actionID, int deviceID) {
    return prepareRestApiRequest(actionID, deviceID, "");
}

/**
 * prepareRestApiRequest
 * Funkcja zwraca gotowy adres zapytania do REST-API
 * @var String action
 * @var int deviceID
 * @var String deviceValue
 */
String prepareRestApiRequest(String actionID, int deviceID, String deviceValue) {
    return prepareRestApiRequest(actionID, String(deviceID), deviceValue);
}

/**
 * prepareRestApiRequest
 * Funkcja zwraca gotowy adres zapytania do REST-API
 * @var String action
 * @var String deviceID
 * @var String deviceValue
 */
String prepareRestApiRequest(String actionID, String deviceID, String deviceValue) {
    String returnUrl = "";

    String action = "";
    if (actionID != NULL && actionID != "") {
        action = "?action=" + actionID;
    }

    String device = "";
    if (deviceID != NULL && deviceID != "") {
        device = "&id=" + String(deviceID);
    }

    String value = "";
    if (deviceValue != NULL && deviceValue != "") {
        value = "&value=" + deviceValue;
    }

    return String(REST_API) + action + device + value;
}

/*******************************
  ARDUINO UNO CONTROLES FUNCTIONS
 *******************************/

/**
 * UnoSwitchLight
 * Instrukcje sterujące oświetleniem.
 * @var int lightId
 */
void UnoSwitchLight(int lightId) {
    if (digitalRead(lightId) == LOW) {
        digitalWrite(lightId, HIGH);
    } else {
        digitalWrite(lightId, LOW);
    }
}

/**
 * UnoSwitchShutter
 * Instrukcje sterujące silnikiem.
 * @var int shutterId
 */
void UnoSwitchShutter(int shutterId) {
    UnoSwitchLight(8);
    /* Instrukcje sterujące */
    delay(3000);
    UnoSwitchLight(8);
}

/*******************************
  OTHER DEBUG FUNCTIONS
 *******************************/

void dump(String var) {
    if (DEBUG) {
        Serial.println("#################### " + var);
    }
}

void dump(int var) {
    return dump(String(var));
}

void printJsonMessage() {
    for (int i = 0; i < strlen(globalJsonResponse); i++) {
        Serial.print(globalJsonResponse[i]);
    }
    Serial.println("");
}