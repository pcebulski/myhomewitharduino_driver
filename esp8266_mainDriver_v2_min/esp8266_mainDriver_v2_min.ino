#include <SoftwareSerial.h>
#include <ArduinoJson.h>

#define DEBUG                       true            // tryb testowy
#define RESPONSE_TIMEOUT            2000            // czas oczekiwania na odpowiedz
#define SM_TIMEOUT                  3000
#define WIFI_CONNECT_TIMEOUT        12000

SoftwareSerial ESP8266(2, 3); // RX=2, TX=3 dla ESP8266

/**
 * Dostępne urządzenia
 */
String devicesTypes [3] = {
    {"Light"},
    {"Shutter"},
    {"MotorStatus"}
};
int devicesArraySize = 9;
int motorPinStatus = 12;
int devicesArray [9][2] = {
    {4, 0},
    {5, 0},
    {6, 0},
    {7, 0},
    {8, 1},
    {9, 1},
    {10, 1},
    {11, 1},
    {motorPinStatus, 2}
};
const byte jsonResponseSize = 200;
char jsonResponse[jsonResponseSize];
boolean newData = false;
String espResponse = "";

/**
 * Ustawienia WiFI oraz REST-API
 */
const char * MY_SSID = "";
const char * MY_PASSWORD = "";
const char * HOST = "192.168.1.110";
const char * REST_API = "/app/rest-api";

void setup() {
    Serial.begin(9600);
    ESP8266.begin(9600);

    for (int i = 0; i < devicesArraySize; i++) {
        pinMode(devicesArray[i][0], OUTPUT);
        digitalWrite(devicesArray[i][0], LOW);
    }

    /* Wstępna konfiguracja ESP-01*/
    sendToWifi("AT+RST"); // reset modulu
    sendToWifi("AT+CWMODE=1"); // configure as access point
    sendToWifi("AT+CIFSR"); // get ip address
    connectEspToWiFi();
}

void loop() {
    devicesController();
}

/**
 * Kontroller instrukcji Arduino Uno
 * @var String message
 * @return void
 */
void unoCommandController(String message) {
    if (find(message, "AT")) {
        // Testowanie komend AT wpisanych z monitora portu szeregowego Arduino UNO.
        String espCommand = message;
        sendToWifi(espCommand);
    } else if (find(message, "ESP+")) {
        // Testowanie komend dla ESP wpisanych z monitora portu szeregowego Arduino Uno
        String espCommand = message.substring(4, message.length());
        ESP8266.println(espCommand);
    } else if (find(message, "UNO+")) {
        // Testowanie komend dla Arduino Uno wpisanych z monitora portu szeregowego Arduino Uno
        String unoCommand = message.substring(4, message.length());
        if (find(unoCommand, "LIGHT+")) { // ŚWIATŁA
            String lightId = unoCommand.substring(6, unoCommand.length());
            UnoSwitchLight(lightId.toInt());
        } else if (find(unoCommand, "SHUTTER+")) { // ROLETY
            String shutterId = unoCommand.substring(8, unoCommand.length());
            UnoSwitchShutter(shutterId.toInt());
        }
    }
}

/**
 * Kontroller instrukcji ESP-01
 * @var String message
 * @return void
 */
void ESP8266CommandController(String message) {
    if (find(message, "AT")) {
        String espResult = sendToWifi(message);
        Serial.println(espResult);
    } else if (find(message, "HELLO")) { //receives HELLO from wifi
        Serial.println("HI!"); //arduino says HI
    }
}

/**
 * Przygotowanie i sprawdzenie połączenia z siecią WiFi
 */
void connectEspToWiFi() {
    sendToWifi("AT+PING=\"" + String(HOST) + "\"");
    if (find(espResponse, "OK")) {
    } else {
        sendToWifi("AT+CWJAP=\"" + String(MY_SSID) + "\",\"" + String(MY_PASSWORD) + "\"");
        delay(WIFI_CONNECT_TIMEOUT);
        connectEspToWiFi();
    }
}

void devicesController() {
    if (prepareConnectionTCP()) {
        getFromRestApi("getAllDevices", "", "");
        // Deserializacja
        StaticJsonDocument<200> doc;
        DeserializationError error = deserializeJson(doc, jsonResponse);

        if (error) {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
            return;
        }

        for (int i = 0; i < devicesArraySize; i++) {
            int pin = doc[i]["p"];
            String val = doc[i]["v"];

            if (devicesTypes[devicesArray[i][1]] == "Light") {
                Serial.println("Światło PIN: " + String(devicesArray[i][0]) + " = " + String(val));
                UnoSwitchLight(devicesArray[i][0], val);
            } else if (devicesTypes[devicesArray[i][1]] == "Shutter") {
                Serial.println("Roleta PIN: " + String(devicesArray[i][0]) + " = " + String(val));
                UnoSwitchShutter(devicesArray[i][0], val);
            }
        }
    }
}

/**
 * Odczyt z REST-API
 */
void getFromRestApi(String methot, String param1) {
    getFromRestApi(methot, param1, "");
}

/**
 * Odczyt z REST-API
 */
void getFromRestApi(String methot, String param1, String param2) {
    String request = "GET " + prepareRestApiRequest(methot, param1, param2);
    String len = String(request.length() + 2);
    ESP8266.println("AT+CIPSEND=" + len);
    if (ESP8266.find(">")) {
        ESP8266.println(request);
        if (ESP8266.find("SEND OK")) {
            readJsonMessage();
        }
    }
}

/**
 * Nawiązanie połączenia TCP z REST-API
 */
boolean prepareConnectionTCP() {
    ESP8266.println("AT+CIPSTART=\"TCP\",\"" + String(HOST) + "\",80");
    String espResponse = readWifiSerialMessage();
    if (find(espResponse, "OK")) {
        return true;
    } else if (find(espResponse, "ALREADY CONNECTED")) {
        return true;
    } else if (find(espResponse, "CONNECT")) {
        return true;
    } else {
        return false;
    }
}

/*
 * Name: find
 * Porównaj ciagi znakow
 * @var String inString
 * @var String findValue
 * @return bool
 */
boolean find(String inString, String findValue) {
    return inString.indexOf(findValue) >= 0;
}

/**
 * Odczytuje z ESP znak po znaku i zwraca połączoną wiadomość
 * @return String
 */
String readWifiSerialMessage() {
    String response = "";
    long int time = millis();
    while ((time + SM_TIMEOUT) > millis()) {
        while (ESP8266.available()) {
            char c = ESP8266.read();
            response += c;
        }
    }
    response.trim();
    return response;
}

/**
 * Funkcja odczytuje jsona z aplikacji WWW
 */
void * readJsonMessage() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;
    long int time = millis();
    bool stopLoop = false;
    while ((time + RESPONSE_TIMEOUT) > millis() && !stopLoop) {
        while (ESP8266.available() > 0) {
            rc = ESP8266.read();
            if (recvInProgress == true) {
                if (rc != endMarker) {
                    jsonResponse[ndx] = rc;
                    ndx++;
                    if (ndx >= jsonResponseSize) {
                        ndx = jsonResponseSize - 1;
                    }
                } else {
                    jsonResponse[ndx] = '\0'; // terminate the string
                    recvInProgress = false;
                    ndx = 0;
                    stopLoop = true;
                }
            } else if (rc == startMarker) {
                recvInProgress = true;
            }
        }
    }
}

/**
 * sendToWifi
 * Funkcja wysyls informacje do ESP8266 i odczytuje odpowiedz z serialportu ESP8266
 * @var String espCommand
 * @return String espResponse
 */
String sendToWifi(String espCommand) {
    espResponse = "";
    ESP8266.println(espCommand);
    long int time = millis();
    while ((time + SM_TIMEOUT) > millis()) {
        while (ESP8266.available()) {
            char c = ESP8266.read();
            espResponse += c;
        }
    }
    espResponse.trim();
    return espResponse;
}

/**
 * prepareRestApiRequest
 * Funkcja zwraca gotowy adres zapytania do REST-API
 * @var String action
 * @var String deviceID
 * @var String deviceValue
 * @return String 
 */
String prepareRestApiRequest(String actionID, String deviceID, String deviceValue) {

    String action = "";
    if (actionID != NULL && actionID != "") {
        action = "?action=" + actionID;
    }

    String device = "";
    if (deviceID != NULL && deviceID != "") {
        device = "&id=" + String(deviceID);
    }

    String value = "";
    if (deviceValue != NULL && deviceValue != "") {
        value = "&value=" + deviceValue;
    }

    return String(REST_API) + action + device + value;
}

/*******************************
  ARDUINO UNO CONTROLES FUNCTIONS
 *******************************/

/**
 * UnoSwitchLight
 * Instrukcje sterujące oświetleniem.
 * @var int pinOut
 */
void UnoSwitchLight(int pinOut) {
    if (digitalRead(pinOut) == LOW) {
        digitalWrite(pinOut, HIGH);
    } else {
        digitalWrite(pinOut, LOW);
    }
}

/**
 * UnoSwitchLight
 * Instrukcje sterujące oświetleniem.
 * @var int pinOut
 * @var String val
 */
void UnoSwitchLight(int pinOut, String val) {
    if (digitalRead(pinOut) == LOW && (val == "on" || val == "\"on\"")) {
        digitalWrite(pinOut, HIGH);
    } else if (digitalRead(pinOut) == HIGH && (val == "off" || val == "\"off\"")) {
        digitalWrite(pinOut, LOW);
    }
}

/**
 * UnoSwitchShutter
 * Instrukcje sterujące silnikiem.
 * @var int pinOut
 */
void UnoSwitchShutter(int pinOut) {
    UnoSwitchLight(pinOut);
    /* Instrukcje sterujące */
    delay(1000);
    UnoSwitchLight(pinOut);
    delay(100);
}

/**
 * UnoSwitchShutter
 * Instrukcje sterujące silnikiem.
 * @var int pinOut
 * @var String val
 */
void UnoSwitchShutter(int pinOut, String val) {
    if (digitalRead(pinOut) == LOW && (val == "open" || val == "\"open\"")) {
        digitalWrite(pinOut, HIGH);
        UnoSwitchShutter(motorPinStatus);
    } else if (digitalRead(pinOut) == HIGH && (val == "close" || val == "\"close\"")) {
        digitalWrite(pinOut, LOW);
        UnoSwitchShutter(motorPinStatus);
    }
}